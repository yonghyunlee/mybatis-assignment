package io.wisoft.seminar;

import io.wisoft.seminar.student.SimpleStudentService;
import io.wisoft.seminar.student.Student;
import io.wisoft.seminar.student.StudentService;
import org.apache.commons.lang3.time.StopWatch;

import java.util.Arrays;
import java.util.List;

public class Application {

  public static void main(String[] args) {

    System.out.println("Hello, App!");
    final StopWatch stopWatch = new StopWatch();
    final StudentService service = new SimpleStudentService();

    int count = 0;
    List<Student> studentList;
    System.out.println();

    System.out.println("전체 사용자 목록 조회");
    studentList = service.getStudentAll();
    studentList.forEach(System.out::println);
    System.out.println();

    System.out.println("여러 사용자 목록 조회");
    List<String> noList = Arrays.asList("20110101", "20110201");
    studentList = service.getStudentListByNo(noList);
    studentList.forEach(System.out::println);
    System.out.println();

//    System.out.println("사용자 등록");
//    final Student student = new Student("20111201", "테스트", "2000-01-01");
//    int count = service.insertStudent(student);
//    System.out.println(count + "행이 등록되었습니다.");
//    System.out.println();

//    System.out.println("사용자 목록 등록 1");
//    final List<Student> newStudentList1 = Arrays.asList(
//      new Student("20130101", "테스트1", "2000-01-01"),
//      new Student("20130201", "테스트2", "2000-01-01"),
//      new Student("20130301", "테스트3", "2000-01-01"),
//      new Student("20130401", "테스트4", "2000-01-01")
//    );
//
//    final StopWatch stopWatch = new StopWatch();
//    stopWatch.start();
//    count = service.insertStudentList(newStudentList1);
//    stopWatch.stop();
//    System.out.println(count + "행이 등록되었습니다. [소요시간] " + stopWatch.getNanoTime());
//    System.out.println();

    System.out.println("사용자 목록 등록 2");
    final List<Student> newStudentList2 = Arrays.asList(
        new Student("20140101", "테스트1", "2000-01-01"),
        new Student("20140201", "테스트2", "2000-01-01"),
        new Student("20140301", "테스트3", "2000-01-01"),
        new Student("20140401", "테스트4", "2000-01-01")
    );
    stopWatch.reset();
    stopWatch.start();
    count = service.insertStudentListBatch(newStudentList2);
    stopWatch.stop();
    System.out.println(count + "행이 등록되었습니다. [소요시간] " + stopWatch.getNanoTime());
    System.out.println();
  }
}
