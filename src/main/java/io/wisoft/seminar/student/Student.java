package io.wisoft.seminar.student;

import java.util.StringJoiner;

public class Student {

  private String no;
  private String name;
  private String birthday;

  public Student() {
  }

  public Student(String no, String name, String birthday) {
    this.no = no;
    this.name = name;
    this.birthday = birthday;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Student.class.getSimpleName() + "[", "]")
        .add("no='" + no + "'")
        .add("name='" + name + "'")
        .add("birthday='" + birthday + "'")
        .toString();
  }
}
